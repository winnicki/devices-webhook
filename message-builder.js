const spreadsheetReader = require('./spreadsheet-reader.js');
const baseMessage = require('./base-message.json');

const deviceCategories = ['Android', 'Apple', 'Blackberry', 'Windows', 'MAC - Laptops', 'WIN - Laptops'];

class Message {
    json = JSON.parse(JSON.stringify(baseMessage));

    setTitle(title) {
        this.json.attachments[0].content.title = title;
    }

    setText(text) {
        this.json.attachments[0].content.text = text;
    }

    addSection(title = '') {
        let section = {
            'activityTitle': title,
            'facts': []
        };
        this.json.attachments[0].content.sections.push(section);
    }

    addFact(name, value, sectionIndex = 0) {
        this.json.attachments[0].content.sections[sectionIndex].facts.push({
            'name': name,
            'value': value
        });
    }

    getJSONString() {
        return JSON.stringify(this.json);
    }
}

async function buildHelpMessage() {
    let message = new Message();

    message.setTitle('Available Commands');
    message.addSection();
    message.addFact('View available commands', '@Devices help');
    message.addFact('Show google sheet url', '@Devices file');
    message.addFact('List all teams', '@Devices teams');
    message.addFact('List devices for a team', '@Devices {team}');
    message.addFact('Get info about device', '@Devices find {deviceId} / @Devices info {deviceId}');
    message.addFact('Take device', '@Devices take {deviceId}');
    message.addFact('Return device', '@Devices return {deviceId}');

    return message.getJSONString();
}

async function buildFileMessage() {
    let message = new Message();

    message.setText('Spreadsheet can be found [here](https://docs.google.com/spreadsheets/d/10FWLrhDlku5kgjkJnS5D-w8yiYJnUEnqhUNpc5cyWHA/edit#gid=1103337103)');

    return message.getJSONString();
}

async function buildInvalidCommandMessage(command) {
    let message = new Message();

    message.setTitle(`\'${command}\' is not a valid command.`);
    message.setText('Look at **@Devices help** for a list of valid commands.')

    return message.getJSONString();
}

async function buildTeamsMessage() {
    let message = new Message();

    message.setTitle("Teams");
    message.addSection();
    message.addFact('QA Team', '@Devices qa');
    message.addFact('Mobile Team', '@Devices mobile');
    message.addFact('FrontEnd Team', '@Devices frontend');
    message.addFact('Creation Team', '@Devices creation');
    message.addFact('Toronto Team', '@Devices toronto');
    message.addFact('Quebec Team', '@Devices quebec');

    return message.getJSONString();
}

async function buildTeamDevicesMessage(team) {
    let message = new Message();

    let title = await spreadsheetReader.getTeamTitle(team);
    let devices = await spreadsheetReader.getTeamDevices(team);

    message.setTitle(`Devices from ${title} Team`)

    let sectionIndex = -1;
    devices.forEach(device => {
        if (deviceCategories.includes(device.ID)) {
            sectionIndex++;
            message.addSection(device.ID);
        } else {
            message.addFact(device.Model, device.ID, sectionIndex);
        }
    });

    return message.getJSONString();
}

async function buildDeviceInfoMessage(deviceId) {
    var message = new Message();

    if (isDeviceIdValid(deviceId)) {
        let [sheet, device] = await spreadsheetReader.findDevice(deviceId);

        if (device === undefined) {
            message.text = `Device ${deviceId} was not found.`;
        } else {
            message.addSection();
            message.addFact('Team', sheet.title);
            message.addFact('Device Id', device.ID);
            message.addFact('Brand', device['Brand']);
            message.addFact('Model', device.Model);
            message.addFact('Os version', device['OS version']);
            message.addFact('Size', device.Size);
            message.addFact('Details', device['Color/Details']);
            message.addFact('Current user', device['Current User']);
            message.addFact('Date out', device['Date Out']);
            message.addFact('Last user', device['Last User']);
            message.addFact('Date in', device['Date In']);
            message.addFact('Notes', device['Note']);
        }
    } else {
        message.setText() = 'Please provide a device id.';
    }

    return message.getJSONString();
}

async function buildTakeDeviceMessage(deviceId, userName) {
    let message = new Message();

    if (isDeviceIdValid(deviceId)) {
        let [sheet, device, success] = await spreadsheetReader.takeDevice(deviceId, userName);

        if (success) {
            message.setText(`${userName} took device ${deviceId} from Team ${sheet.title}.`);
        } else {
            if (device === undefined) {
                message.setText(`Device ${deviceId} was not found.`);
            } else {
                message.setText(`Cannot take device ${deviceId} from Team ${sheet.title}. Please ask ${device['Current User']} to return it first.`);
            }
        }
    } else {
        message.setText() = 'Please provide a device id.';
    }

    return message.getJSONString();
}

async function buildReturnDeviceMessage(deviceId, userName) {
    let message = new Message();

    if (isDeviceIdValid(deviceId)) {
        let [sheet, device, success] = await spreadsheetReader.returnDevice(deviceId, userName);

        if (success) {
            message.setText(`${userName} returned device ${deviceId}`);
        } else {
            if (device === undefined) {
                message.setText(`Device ${deviceId} was not found.`);
            } else {
                if (device['Current User'] == '') {
                    message.setText(`Device ${deviceId} from team ${sheet.title} already returned.`);
                } else {
                    message.setText(`Device ${deviceId} from team ${sheet.title} is currently with ${device['Current User']}.`);
                }
            }
        }
    } else {
        message.setText() = 'Please provide a device id.';
    }

    return message.getJSONString();
}

function isDeviceIdValid(deviceId) {
    return deviceId !== undefined && deviceId != ''
}

module.exports = {
    buildHelpMessage,
    buildFileMessage,
    buildInvalidCommandMessage,
    buildTeamsMessage,
    buildTeamDevicesMessage,
    buildDeviceInfoMessage,
    buildTakeDeviceMessage,
    buildReturnDeviceMessage
}