const {
    GoogleSpreadsheet
} = require('google-spreadsheet');
const creds = require('./google-drive-client-secret.json');
const spreadsheetId = '1YaD63PZCOljGXoYDI_ea-DF7eVgLJqpxnI5Jqia_pO8';
const doc = new GoogleSpreadsheet(spreadsheetId);

const teams = ['QA', 'Mobile', 'FrontEnd', 'Creation', 'Toronto', 'Quebec'];

async function authenticateAndLoadDoc() {
    await doc.useServiceAccountAuth({
        client_email: creds.client_email,
        private_key: creds.private_key
    });
    await doc.loadInfo();
}

async function getSheet(sheetName) {
    try {
        for (let index = 0; index < doc.sheetCount; index++) {
            const sheet = doc.sheetsByIndex[index];
            if (sheet.title.toLowerCase() == sheetName) {
                return sheet
            }
        }
        throw new Error(`Sheet ${sheetName} was not found.`);
    } catch (err) {
        console.log(err);
    }
}

async function getTeamTitle(team) {
    await authenticateAndLoadDoc();
    let sheet = await getSheet(team);
    return sheet.title;
}

async function getTeamDevices(team) {
    await authenticateAndLoadDoc();
    let sheet = await getSheet(team);
    let rows = await sheet.getRows();
    return rows;
}

async function findDevice(deviceId) {
    await authenticateAndLoadDoc();
    for (let index = 0; index < doc.sheetCount; index++) {
        let sheet = doc.sheetsByIndex[index];
        if (teams.includes(sheet.title)) {
            let rows = await sheet.getRows();
            let device = rows.find(row => {
                return row.ID == deviceId
            })
            if (device !== undefined) {
                return [sheet, device];
            }
        }
    }
    return [undefined, undefined];
}

async function takeDevice(deviceId, userName) {
    let [sheet, device] = await findDevice(deviceId);
    let success = false;

    if (device !== undefined) {
        if (device['Current User'] == '') {
            device['Current User'] = userName;
            device['Date Out'] = getFormattedDate();
            device.save();
            success = true;
        }
    }
    return [sheet, device, success];
}

async function returnDevice(deviceId, userName) {
    let [sheet, device] = await findDevice(deviceId);
    let success = false;

    if (device !== undefined) {
        let currentUser = device['Current User'];
        if (currentUser == userName) {
            device['Current User'] = '';
            device['Date Out'] = '';
            device['Last User'] = userName;
            device['Date In'] = getFormattedDate();
            device.save();
            success = true;
        }
    }
    return [sheet, device, success];
}

function getFormattedDate() {
    return new Date().toISOString()
        .replace(/T/, ' ')
        .replace(/\..+/, '');
}

module.exports = {
    getTeamTitle,
    getTeamDevices,
    findDevice,
    takeDevice,
    returnDevice
}