const crypto = require('crypto');
const http = require('http');
const {
    getResponseFromNurunDevices
} = require('./nurun-devices.js');

const tokenTeamNMBots = 'cR5Jf2U8Ngk81Whs03S8NujZjzC5lbcvspraPLm+Ovg=';

const microsoftTeamsOutgoingWebhookToken = tokenTeamNMBots;
const bufferSecret = Buffer(microsoftTeamsOutgoingWebhookToken, 'base64');
const PORT = process.env.port || process.env.PORT || 8080;

http.createServer(function (request, response) {
    var payload = '';

    request.on('data', function (data) {
        payload += data;
    });

    request.on('end', function () {
        try {
            writeSuccessfulResponse(this, payload, response);
        } catch (err) {
            response.writeHead(400);
            return response.end('Error: ' + err + '\n' + err.stack);
        }
    });
}).listen(PORT);

async function writeSuccessfulResponse(incomingMessage, payload, response) {
    var auth = incomingMessage.headers['authorization'];
    var msgBuf = Buffer.from(payload, 'utf8');
    var msgHash = 'HMAC ' + crypto.createHmac('sha256', bufferSecret).update(msgBuf).digest('base64');

    response.writeHead(200);
    if (msgHash === auth) {
        var responseMessage = await getResponseFromNurunDevices(payload);;
    } else {
        var responseMessage = '{ "type": "message", "text": "Error: message sender cannot be authenticated." }';
    }

    response.write(responseMessage);
    response.end();
}