const messageBuilder = require('./message-builder.js');

const teamNames = ['qa', 'mobile', 'frontend', 'creation', 'toronto', 'quebec'];
const testCommands = {
    'noCommand': '{ "text": "", "from": { "name": "David Winnicki" } }',
    'help': '{ "text": "help", "from": { "name": "David Winnicki" } }',
    'file': '{ "text": "file", "from": { "name": "David Winnicki" } }',
    'teams': '{ "text": "teams", "from": { "name": "David Winnicki" } }',
    'teamDevices': '{ "text": "qa", "from": { "name": "David Winnicki" } }',
    'findTrue': '{ "text": "find TOR1", "from": { "name": "David Winnicki" } }',
    'findFalse': '{ "text": "find FAKE1234", "from": { "name": "David Winnicki" } }',
    'take': '{ "text": "take MA01", "from": { "name": "David Winnicki" } }',
    'return': '{ "text": "return MA015", "from": { "name": "David Winnicki" } }'
}

module.exports = {
    getResponseFromNurunDevices
}

async function getResponseFromNurunDevices(payload) {
    let [command, userName] = parsePayload(payload);

    if (command == '' || command == 'help') {
        return messageBuilder.buildHelpMessage();
    } else if(command == 'file') {
        return messageBuilder.buildFileMessage();
    } else if (command == 'teams') {
        return messageBuilder.buildTeamsMessage();
    } else if (teamNames.includes(command)) {
        return await messageBuilder.buildTeamDevicesMessage(command);
    } else if (command.startsWith('info') || command.startsWith('find')) {
        return await messageBuilder.buildDeviceInfoMessage(parseDeviceId(command));
    } else if (command.startsWith('take')) {
        return await messageBuilder.buildTakeDeviceMessage(parseDeviceId(command), userName);
    } else if (command.startsWith('return')) {
        return await messageBuilder.buildReturnDeviceMessage(parseDeviceId(command), userName);
    } else {
        return messageBuilder.buildInvalidCommandMessage(command);
    }
}

function parsePayload(payload) {
    let command = JSON.parse(payload).text.replace('<at>Devices</at>', '').trim().replace('&nbsp;', '').toLowerCase();
    let userName = JSON.parse(payload).from.name;
    return [command, userName];
}

function parseDeviceId(command) {
    let splitString = command.split(' ');
    if(splitString[1] == undefined) {
        return '';
    } else {
        return splitString[1].toUpperCase();
    }
}